(defproject hello-hugsql "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[com.layerware/hugsql "0.5.1"]
                 [environ "1.2.0"]
                 [hikari-cp "2.13.0"]
                 [org.clojure/clojure "1.10.1"]
                 [org.postgresql/postgresql "42.2.18"]]
  :plugins [[lein-dotenv "RELEASE"]]
  :main ^:skip-aot hello-hugsql.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
