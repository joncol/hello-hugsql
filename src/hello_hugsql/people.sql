-- :name create-people-table :!
CREATE TABLE people (
    id SERIAL,
    name varchar,
    age int,
    v vehicle
)

-- :name people :? :*
SELECT * FROM people

-- :name add-person :! :n
INSERT INTO people (name, age, v) VALUES (:name, :age, :vehicle::vehicle)

-- :name set-name :! :n
UPDATE people SET name = :name WHERE id = :id

-- :name update-name :<! :1
UPDATE people SET name = :name WHERE id = :id returning (name, age)

-- :name get-people-by-vehicle :? :*
select * from people where v in (/*~
(->> (:vehicle params)
     (map #(str \' % \' "::vehicle"))
     (clojure.string/join ", "))
~*/)
