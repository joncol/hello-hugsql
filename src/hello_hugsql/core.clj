(ns hello-hugsql.core
  (:gen-class)
  (:require [environ.core :refer [env]]
            [hikari-cp.core :as hikari]
            [hugsql.core :as hugsql]
            [clojure.string :as str]))

(hugsql/def-db-fns "hello_hugsql/people.sql")

(hugsql/def-sqlvec-fns "hello_hugsql/people.sql")

(get-people-by-vehicle-sqlvec {:vehicle ["car" "skateboard"]})

;; (str \( (->> ["car" "skateboard"]
;;              (map #(str \' % \' "::vehicle"))
;;              (clojure.string/join ", ")) \))

(defn- pool-config []
  {:auto-commit        true
   :read-only          false
   :connection-timeout 30000
   :validation-timeout 5000
   :idle-timeout       600000
   :max-lifetime       1800000
   :minimum-idle       10
   :maximum-pool-size  10
   :pool-name          "db-pool-bull"
   :adapter            "postgresql"
   :username           (or (env :db-user) "test-user")
   :password           (or (env :db-password) "")
   :database-name      (or (env :db-database) "test")
   :server-name        (or (env :db-host) "localhost")
   :port-number        (or (env :db-port) 5432)
   :register-mbeans    false})

(defonce db {:datasource (hikari/make-datasource (pool-config))})

;; Run in psql:
;; create type vehicle as enum('car', 'bike', 'skateboard');

(comment
  (create-people-table db)

  (add-person db {:name "Jonas" :age 41 :vehicle "car"})

  (add-person db {:name "Martin" :age 39 :vehicle nil})

  (add-person db {:name "Andreas" :age 31 :vehicle "bike"})

  (add-person db {:name "Lo" :age 11 :vehicle "skateboard"})

  (update-name db {:id 1 :name "JC"})

  (people db)

  (get-people-by-vehicle db {:vehicle ["car" "skateboard"]})

  (get-people-by-vehicle db {:vehicle ["car" "bike"]})

  )

(defn -main [& args]
  (println "Hello, World!"))
